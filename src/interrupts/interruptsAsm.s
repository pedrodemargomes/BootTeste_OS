.set IRQ_BASE, 0x20

.section .text
.extern keyboard_handler_main
.global keyboard_handler
.global load_idt

.extern rtc_handler_main
.global rtc_handler

keyboard_handler:
	pushal
	call keyboard_handler_main
	popal
	iret

rtc_handler:
	pushal
	call rtc_handler_main
	popal
	iret




