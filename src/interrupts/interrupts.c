#include <stdint.h>

#include <interrupts.h>

#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_STATUS_PORT 0x64
#define IDT_SIZE 256

static char scancode[100] = {0};

uint32_t sysTick = 0;


// ++++++++++ TESTE ++++++++++++++++

void printf(char* str);

// +++++++++++++++++++++++++++++++++



static inline void outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) );
    /* There's an outb %al, $imm8  encoding, for compile-time constant port numbers that fit in 8b.  (N constraint).
     * Wider immediate constants would be truncated at assemble-time (e.g. "i" constraint).
     * The  outb  %al, %dx  encoding is the only option for all other cases.
     * %1 expands to %dx because  port  is a uint16_t.  %w1 could be used if we had the port number a wider C type */
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ( "inb %1, %0"
                   : "=a"(ret)
                   : "Nd"(port) );
    return ret;
}

struct IDT_entry {
	uint16_t offset_lowerbits;
	uint16_t selector;
	uint8_t zero;
	uint8_t type_attr;
	uint16_t offset_higherbits;
}  __attribute__((__packed__));

struct IDT_entry IDT[IDT_SIZE];

void keyboard_handler();

void rtc_handler();

void idt_init(void)
{
	uint32_t keyboard_address;
	uint32_t idt_address;
	uint32_t idt_ptr[2];

	/* populate IDT entry of keyboard's interrupt */
	keyboard_address = (uint32_t)keyboard_handler; 
	IDT[0x21].offset_lowerbits = keyboard_address & 0xffff;
	IDT[0x21].selector = 0x08; /* KERNEL_CODE_SEGMENT_OFFSET */
	IDT[0x21].zero = 0;
	IDT[0x21].type_attr = 0x8e; /* INTERRUPT_GATE */
	IDT[0x21].offset_higherbits = (keyboard_address & 0xffff0000) >> 16;
	
	uint32_t rtc_address;

	rtc_address = (uint32_t)rtc_handler; 
	IDT[0x28].offset_lowerbits = rtc_address & 0xffff;
	IDT[0x28].selector = 0x08; /* KERNEL_CODE_SEGMENT_OFFSET */
	IDT[0x28].zero = 0;
	IDT[0x28].type_attr = 0x8e; /* INTERRUPT_GATE */
	IDT[0x28].offset_higherbits = (rtc_address & 0xffff0000) >> 16;
	

	/*          Ports
	*	     PIC1	PIC2
	*Command 0x20	0xA0
	*Data	 0x21	0xA1
	*/

	/* ICW1 - begin initialization */
	outb(0x20 , 0x11);
	outb(0xA0 , 0x11);

	/* ICW2 - remap offset address of IDT */
	/*
	* In x86 protected mode, we have to remap the PICs beyond 0x20 because
	* Intel have designated the first 32 interrupts as "reserved" for cpu exceptions
	*/
	outb(0x21 , 0x20);
	outb(0xA1 , 0x28);

	/* ICW3 - setup cascading */
	outb(0x21 , 0x00);  
	outb(0xA1 , 0x00);  

	/* ICW4 - environment info */
	outb(0x21 , 0x01);
	outb(0xA1 , 0x01);
	/* Initialization finished */

	/* mask interrupts */
	outb(0x21 , 0xff);
	outb(0xA1 , 0xff);

	/* fill the IDT descriptor */
	idt_address = (uint32_t)IDT ;
	idt_ptr[0] = (sizeof(struct IDT_entry) * IDT_SIZE) + ((idt_address & 0xffff) << 16);
	idt_ptr[1] = idt_address >> 16 ;

	asm volatile ( "lidt (%0)" : : "Nd"(idt_ptr) );
	asm volatile ( "sti" );
}

uint8_t keyboard_handler_main(void) {
	uint8_t status;
	uint8_t keycode;

	// ++++++++ TESTE +++++++
	char str[2];
	str[1] = '\0';
	// ++++++++++++++++++++++

	// write EOI
	outb(0x20, 0x20);

	status = inb(KEYBOARD_STATUS_PORT);
	// Lowest bit of status will be set if buffer is not empty
	if (status & 0x01) {
		keycode = inb(KEYBOARD_DATA_PORT);
		if(keycode < 0)
			return 0x00;
		// +++++++ TESTE ++++++
		str[0] = scancode[keycode]; 
		printf(str);
		// +++++++++++++++++++
		return scancode[keycode];
	}

}

uint8_t rtc_handler_main(void) {
	outb(0x70, 0x0C);	// select register C
	inb(0x71);		// just throw away contents

	// write EOI
	outb(0x20, 0x20);
	outb(0xA0, 0x20);
	
	sysTick++;
}

void kb_init(void)
{
	scancode[0x02] = '1';
	scancode[0x03] = '2';
	scancode[0x04] = '3';
	scancode[0x05] = '4';
	scancode[0x06] = '5';
	scancode[0x07] = '6';
	scancode[0x08] = '7';
	scancode[0x09] = '8';
	scancode[0x0A] = '9';
	scancode[0x0B] = '0';

	scancode[0x10] = 'q';
	scancode[0x11] = 'w';
	scancode[0x12] = 'e';
	scancode[0x13] = 'r';
	scancode[0x14] = 't';
	scancode[0x15] = 'y';
	scancode[0x16] = 'u';
	scancode[0x17] = 'i';
	scancode[0x18] = 'o';
	scancode[0x19] = 'p';

	scancode[0x1E] = 'a';
	scancode[0x1F] = 's';
	scancode[0x20] = 'd';
	scancode[0x21] = 'f';
	scancode[0x22] = 'g';
	scancode[0x23] = 'h';
	scancode[0x24] = 'j';
	scancode[0x25] = 'k';
	scancode[0x26] = 'l';

	scancode[0x2C] = 'z';
	scancode[0x2D] = 'x';
	scancode[0x2E] = 'c';
	scancode[0x2F] = 'v';
	scancode[0x30] = 'b';
	scancode[0x31] = 'n';
	scancode[0x32] = 'm';

	scancode[0x1C] = '\n';
	scancode[0x39] = ' ';


	/* 0xFD is 11111101 - enables IRQ1 (keyboard)*/
	uint8_t prevIrq = inb(0x21);	
	outb(0x21 , 0xFD & prevIrq );
}

void rtc_init(void) {

	asm volatile ( "cli" ); 
		
	uint8_t rate = 0x05;

	outb(0x70, 0x8B);	
	char prev = inb(0x71);
	outb(0x70, 0x8B);
	outb(0x71, prev | 0x40);

	outb(0x70, 0x8A);
	prev = inb(0x71);
	outb(0x70, 0x8A);
	outb(0x71, (prev & 0xF0) | rate );
	outb(0x70, 0x0C);
	inb(0x71);

	// PIC 2
	// 0xFE is 11111110 - enables IRQ0 (rtc)
	uint8_t prevIrq = inb(0xA1);
	outb(0xA1 , 0xFE & prevIrq );

	// PIC 1
	// 0xFB is 11111011 - enables IRQ2 (rtc)
	prevIrq = inb(0x21);	
	outb(0x21 , 0xFB & prevIrq );

	outb(0x70, 0x0C);	// select register C
	inb(0x71);		// just throw away contents


	asm volatile ( "sti" );
	
}


