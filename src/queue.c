#include <stdio.h>
#include "queue.h"

void queue_append (queue_t **queue, queue_t *elem) {
	// Verificacoes de erro
	if( (queue == NULL) || (elem == NULL) || (elem->next != NULL) || (elem->prev != NULL) ) {
		return ;
	}
	// se a fila estiver vazia
	if( *queue == NULL) {
		elem->next = elem;
		elem->prev = elem;
		*queue = elem;
		return ;
	}

	queue_t *ultimo = (*queue)->prev;
	elem->next = *queue;
	elem->prev = ultimo;
	ultimo->next = elem;
	(*queue)->prev = elem;

	return ;
}

queue_t *queue_remove(queue_t **queue, queue_t *elem) {
	if( (queue == NULL) || (elem == NULL) || (elem->next == NULL) || (elem->prev == NULL) || (*queue == NULL) ) {
		return NULL;
	}
	queue_t *t = *queue;
	while( (t != elem) && (t->next != *queue) ) {
		t = t->next;
	}
	if(t != elem) {
		return NULL;
	}
	if(t == *queue) {
		if(t->next == t) {
			*queue = NULL;
			t->next = t->prev = NULL;
			return t;
		} else {
			*queue = (**queue).next;
		}	
	}
	t->prev->next = t->next;
	t->next->prev = t->prev;

	t->next = t->prev = NULL;
	return t;
}

void queue_print (char *name, queue_t *queue, void print_elem (void*) ) {
	printf("%s [",name);	
	if(queue == NULL) {
		printf("]\n");		
		return ;
	}	

	queue_t *t = queue;
	print_elem(t);
	t = t->next;
	while(t != queue) {
		printf(" ");
		print_elem(t);
		t = t->next;	
	}
	printf("]\n");
	return ;
}

int queue_size (queue_t *queue) {
	if(queue == NULL) {
		return 0;
	}
	int count = 1;
	queue_t *t = queue->next;
	while(t != queue) {
		t = t->next;
		count++;
	}
	return count;
}

