
#ifndef __KERNEL_DATA__
#define __KERNEL_DATA__

#include "queue.h"		// biblioteca de filas genéricas

typedef struct context_t
{
	// General registers
	uint32_t eax;
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;

	uint32_t eip;

} context_t;

// Estrutura que define um Task Control Block (TCB)
typedef struct task_t
{
	struct task_t *prev, *next ;		// ponteiros para usar em filas
	int id ;				// identificador da tarefa
	context_t context ;			// contexto armazenado da tarefa
	void *stack ;			// aponta para a pilha da tarefa
	int prioEst;	
	int prioDin;
	int sysFlag;
	unsigned int activations;
	unsigned int timeInit;
	unsigned int cpuTime;
	short int status;	
	struct task_t *filaJoin;
	short int joinReturnCode;
	int awakeTime;
	//disk_t disco;
	// ... (outros campos serão adicionados mais tarde)
} task_t ;

// estrutura que define um semáforo
typedef struct
{
	int value;
	int ativo;
	struct task_t *filaSuspenso;
	// preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
	// preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
	// preencher quando necessário
} barrier_t ;


// estrutura que define a mensagem
typedef struct
{
	struct mensagem *prev, *next ;
	void *msg;
} mensagem_t;

// estrutura que define uma fila de mensagens
typedef struct
{
	mensagem_t *fila;
	semaphore_t mutBuff;
	semaphore_t semEspVazio;
	semaphore_t semItens;
	int tamElem;
	int maxTamFila;
} mqueue_t ;

#endif


